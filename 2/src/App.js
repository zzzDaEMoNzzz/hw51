import React, { Component } from 'react';
import Logo from './logo.svg';
import './App.css';

class AppHeader extends Component {
  render() {
    return (
      <header className="appHeader">
        <nav>
          <a href="#" className="logo"><img src={Logo} alt=""/>HomeWork51</a>
          <a href="#">About</a>
          <a href="#">Work</a>
          <a href="#">Contact</a>
        </nav>
      </header>
    );
  }
}

class SideBar extends Component {
  render() {
    return (
      <div className="sideBar">
        <ul>
          <li><a href="#">Lorem ipsum dolor sit amet.</a></li>
          <li><a href="#">Lorem ipsum dolor.</a></li>
          <li><a href="#">Lorem ipsum dolor sit amet.</a></li>
          <li><a href="#">Some link</a></li>
          <li><a href="#">Another link</a></li>
        </ul>
      </div>
    );
  }
}

class AppBody extends Component {
  render() {
    return (
      <div className="appBody">
        <div className="mainBlock">
          <h2>Lorem ipsum dolor sit amet.</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, culpa doloremque ipsam laboriosam praesentium quae ratione ut? Dolore minima quasi suscipit! Autem cupiditate, dolor doloribus et, excepturi expedita illo inventore laudantium libero molestiae nostrum odio optio similique soluta vero? Deserunt doloremque error non qui recusandae sed tenetur. Asperiores at, consectetur consequatur culpa cupiditate, exercitationem facere laboriosam maiores modi mollitia omnis repellat vero. Distinctio ducimus eligendi in incidunt, inventore ipsum laboriosam minus molestias mollitia numquam obcaecati rerum suscipit totam voluptate voluptatem.</p>
          <h3>Lorem ipsum dolor.</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet aperiam at blanditiis commodi, consectetur debitis dolorem, ea harum hic magni porro, quae ratione soluta unde? Aperiam atque error facere modi sunt vero! Ab animi, atque consequatur deleniti doloremque ea, eligendi est eum facere illo libero nam sit sunt veritatis?</p>
          <h4>Lorem ipsum dolor sit amet.</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad animi architecto assumenda aut autem corporis culpa cumque delectus enim esse et, illum inventore ipsa libero magnam magni minus mollitia nulla quod recusandae rem repudiandae, saepe sint tempore temporibus voluptatem?</p>
        </div>
        <SideBar />
      </div>
    );
  }
}

class AppFooter extends Component {
  render() {
    return (
      <footer className="appFooter">
        <a href="mailto:daemon.from.kg@gmail.com">Vladimir Kurlov</a>
      </footer>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <AppHeader />
        <AppBody />
        <AppFooter />
      </div>
    );
  }
}

export default App;
