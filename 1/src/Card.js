import React, { Component } from 'react';
import './Card.css';

class Card extends Component {
  render() {
    return (
      <div className='card'>
        <img
          src={this.props.filmPoster}
          alt=""
        />
        <p><span>{this.props.filmName} ({this.props.filmYear})</span></p>
      </div>
    );
  }
}

export default Card;