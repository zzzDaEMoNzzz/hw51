import React, { Component } from 'react';
import Card from './Card';

import b13_img from './images/b13.jpg';
import shawshank_img from './images/shawshank.jpg';
import lotr_img from './images/lotr.jpg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className='cardsBlock'>
          <Card filmName="Banlieue 13" filmYear="2004" filmPoster={b13_img} />
          <Card filmName="The Shawshank Redemption" filmYear="1994" filmPoster={shawshank_img} />
          <Card filmName="The Lord of the Rings: The Fellowship of the Ring" filmYear="2001" filmPoster={lotr_img} />
        </div>
      </div>
    );
  }
}

export default App;


